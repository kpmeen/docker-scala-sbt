########################################################
# Dockerfile for image with Scala, SBT and some common
# dependencies 
#
# https://gitlab.com/kpmeen/docker-scala-sbt
#
# Inspired by https://github.com/sbt/docker-sbt/blob/master/eclipse-temurin/Dockerfile
########################################################

# Input arguments for the script
ARG JAVA_BASE_IMAGE_TAG
ARG SCALA_VERSION
ARG SBT_VERSION

FROM eclipse-temurin:${JAVA_BASE_IMAGE_TAG}

# Set the Scala version to install
ENV SCALA_VERSION=${SCALA_VERSION:-2.13.15}
# Set the SBT version to install
ENV SBT_VERSION=${SBT_VERSION:-1.10.5}

# Changing below requires changes in the "sbtopts" file as well
ENV SBT_HOME="/root/cache/sbt"
ENV COURSIER_CACHE="/root/cache/coursier"
ENV IVY_CACHE="/root/cache/ivy2"

# Prepare sbt cache directories
RUN \
  mkdir -p ${SBT_HOME} && \
  ln -s ${SBT_HOME} ~/.sbt && \
  mkdir -p ${COURSIER_CACHE} && \
  mkdir -p ~/.cache && \
  ln -s ${COURSIER_CACHE} ~/.cache/coursier && \
  mkdir -p ${IVY_CACHE} && \
  ln -s ${IVY_CACHE} ~/.ivy2

# RUN \
#   echo "*****************************************************" && \
#   echo "DEBUG: Using scala version = ${SCALA_VERSION}" && \
#   echo "DEBUG: Using sbt version = ${SBT_VERSION}}" && \
#   echo "DEBUG: Locale is:" && \
#   echo "$(locale)" && \
#   echo "*****************************************************"

# Install additional packages
RUN \
  rm /var/lib/dpkg/info/libc-bin.* && \
  apt-get clean && \
  apt-get update && \
  apt-get install -y libc-bin && \
  # Install git and rpm for sbt-native-packager (see https://github.com/sbt/docker-sbt/pull/114)
  apt-get install -y git && \
  apt-get install -y rpm && \
  # GPG is required for installing docker-engine
  apt-get install -y apt-utils && \
  apt-get install -y apt-transport-https && \
  apt-get install -y ca-certificates && \
  apt-get install -y gnupg-agent && \
  apt-get install -y software-properties-common && \
  rm -rf /var/lib/apt/lists/*

# Install sbt
RUN \
  curl -fsL "https://github.com/sbt/sbt/releases/download/v$SBT_VERSION/sbt-$SBT_VERSION.tgz" | tar xfz - -C /usr/share && \
  chown -R root:root /usr/share/sbt && \
  chmod -R 755 /usr/share/sbt && \
  ln -s /usr/share/sbt/bin/sbt /usr/local/bin/sbt

# Install Scala
RUN \
  case $SCALA_VERSION in \
    "3"*) URL=https://github.com/lampepfl/dotty/releases/download/$SCALA_VERSION/scala3-$SCALA_VERSION.tar.gz SCALA_DIR=/usr/share/scala3-$SCALA_VERSION ;; \
    *) URL=https://downloads.typesafe.com/scala/$SCALA_VERSION/scala-$SCALA_VERSION.tgz SCALA_DIR=/usr/share/scala-$SCALA_VERSION ;; \
  esac && \
  curl -fsL $URL | tar xfz - -C /usr/share && \
  mv $SCALA_DIR /usr/share/scala && \
  chown -R root:root /usr/share/scala && \
  chmod -R 755 /usr/share/scala && \
  ln -s /usr/share/scala/bin/* /usr/local/bin && \
  case $SCALA_VERSION in \
    "3"*) echo '@main def main = println(s"Scala library version ${dotty.tools.dotc.config.Properties.versionNumberString}")' > test.scala ;; \
    *) echo "println(util.Properties.versionMsg)" > test.scala ;; \
  esac && \
  scala -nocompdaemon test.scala && rm test.scala

# Symlink java to have it available on non-root users path
RUN ln -s /opt/java/openjdk/bin/java /usr/local/bin/java

# Install docker
RUN \
  # Add Docker's official GPG key:
  install -m 0755 -d /etc/apt/keyrings  && \
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc  && \
  chmod a+r /etc/apt/keyrings/docker.asc  && \
  # Add the repository to Apt sources:
  echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
    $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
    tee /etc/apt/sources.list.d/docker.list > /dev/null  && \
  apt-get update && \
  # Install Docker packages
  apt-get install -y \
    docker-ce \
    docker-ce-cli \
    docker-buildx-plugin \
    containerd.io && \
  # Start the Docker service
  dockerd -v && \
  docker -v

# Define working directory
WORKDIR /root

# Prepare sbtopts
COPY ./sbtopts /etc/sbt/sbtopts

# Prepare sbt (warm cache)
RUN \
  mkdir -p test-project && \
  cd test-project && \
  sbt sbtVersion && \
  mkdir -p project && \
  echo "scalaVersion := \"${SCALA_VERSION}\"" > build.sbt && \
  echo "sbt.version=${SBT_VERSION}" > project/build.properties && \
  echo "// force sbt compiler-bridge download" > project/Dependencies.scala && \
  echo "case object Temp" > Temp.scala && \
  sbt -v compile && \
  cd .. && \
  rm -rf test-project

# Place entrypoint in /usr/local/bin
COPY ./entrypoint.sh /root/entrypoint.sh
ENTRYPOINT ["./entrypoint.sh"]
